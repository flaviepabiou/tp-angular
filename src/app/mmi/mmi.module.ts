import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MmiPageRoutingModule } from './mmi-routing.module';

import { MmiPage } from './mmi.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MmiPageRoutingModule
  ],
  declarations: [MmiPage]
})
export class MmiPageModule {}
