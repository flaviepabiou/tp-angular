import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MmiPage } from './mmi.page';

const routes: Routes = [
  {
    path: '',
    component: MmiPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MmiPageRoutingModule {}
