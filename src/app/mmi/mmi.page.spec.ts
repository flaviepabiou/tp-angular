import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MmiPage } from './mmi.page';

describe('MmiPage', () => {
  let component: MmiPage;
  let fixture: ComponentFixture<MmiPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MmiPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MmiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
