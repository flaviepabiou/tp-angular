import { Component, OnInit } from '@angular/core';
import { DemoService } from '../services/demo/demo.service';

@Component({
  selector: 'app-news',
  templateUrl: './news.page.html',
  styleUrls: ['./news.page.scss'],
})
export class NewsPage implements OnInit {

  private url="http://newsapi.org/v2/top-headlines?country=fr&category=technology&apiKey=cae6828bb86e439fa1cd7988ee475548";
  public data:Object;
  public isload = false;
  public contentMMI:string;
  constructor(private _demo:DemoService) {
    
   }

  ngOnInit() {
   console.log("Etape 1");
   let result = this._demo.get(this.url);
    result.subscribe (_data =>{
      //console.log(_data.articles[0]);
      console.log("Etape : resultat ok");
      this.isload = true;
      console.log(this.isload);
      this.data = _data.articles;
    });
   }

   

}
