import { Component, OnInit } from '@angular/core';
import { DemoService } from '../services/demo/demo.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss'],
})
export class ContactPage implements OnInit {

  private url = "https://www.prevision-meteo.ch/services/json/montelimar";
  private data: Object;
  constructor(private _demo: DemoService) {
     
  }

    ngOnInit() {// ajout de la météo
      console.log('etape1');
      let result = this._demo.get(this.url);
      result.subscribe(_data => {
        console.log(_data.city_info);
        console.log(_data.current_condition);
        console.log(_data.fcst_day_0);
        this.isload = true;
        console.log(this.isload);
        console.log("Etape resultat=ok");
        this.data = _data;
        this.name = _data.city_info.name;
        this.sunrise = _data.city_info.sunrise;
        this.sunset = _data.city_info.sunset;
        this.condition = _data.current_condition.condition;
        this.tmin = _data.fcst_day_0.tmin;
        this.tmax = _data.fcst_day_0.tmax;
      });
    }
          
  ngAfterViewInit() {
    navigator.geolocation.getCurrentPosition(this.showPosition);
  }

  openCamera(){
    let video = <HTMLVideoElement> document.getElementById("video"); // fonctionne pour ANGULAR
    
    navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
      video.srcObject = stream;
      video.play();
    });
  }

  showPosition(position) {
      console.log("show");
      console.log(position);
      let latlon = position.coords.latitude + "," + position.coords.longitude;
    
      var img_url = "https://maps.googleapis.com/maps/api/staticmap?center="+latlon+"&zoom=14&size=400x300&sensor=false&key=AIzaSyCZJwauN9_GN5zVqvz01bHBjf8zT-97Bw4";
     console.log(img_url);

      let img = <HTMLImageElement> document.getElementById("map");
      img.src=img_url;
  }

  
}
